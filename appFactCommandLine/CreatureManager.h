//
//  CreatureManager.h
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-07.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

//Manager for every being
//Singleton

#import <Foundation/Foundation.h>
#import "Being.h"

@interface CreatureManager : NSObject
@property (nonatomic, strong) NSMutableArray *creatureList;

+(CreatureManager*)sharedInstance;
-(Being*)createBeingWithName:(NSString*)name
                   andHealth:(NSNumber*)health andStrength:(NSNumber*)strength;
-(void)printNameForAllCreatures;

- (void)fetchAllBeings;


@end
