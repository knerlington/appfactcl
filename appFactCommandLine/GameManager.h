//
//  GameManager.h
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-07.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BeingManager.h"

@interface GameManager : NSObject
@property (nonatomic, strong) BeingManager *beingManager; //Manages every being


-(void)createNewGame;
-(void)initialize;

@end
