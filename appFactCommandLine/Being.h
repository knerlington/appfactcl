//
//  Being.h
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-07.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Weapon.h"


@interface Being : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *type;
@property (nonatomic) int health;
@property (nonatomic) int strength;

- (id)initWithDictionary:(NSArray *)array;

-(instancetype)initWithName:(NSString*)name;

-(instancetype)initWith:(int)health
                andName:(NSString*)name
            andStrength:(int)strength;

-(void)attack:(Being*)being;
-(BOOL)isDead;


@end
