//
//  Being.m
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-07.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "Being.h"

@implementation Being

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if(self){
        _name = [dictionary objectForKey:@"name"];
    }
    return self;
}

-(instancetype)initWith:(int)health andName:(NSString *)name andStrength:(int)strength{
    self = [super init];
    if(self){
        _name = name;
        _health = health;
        _strength = strength;

    }
    return self;
}

-(instancetype)initWithName:(NSString *)name{
    self = [super init];
    if(self)
    {
        self.name = name;
        self.health = 10;
        self.strength = 10;
    }
    return self;
}

-(BOOL)isDead{
    BOOL isDead = false;
    if(self.health <= 0){
        isDead = true;
    }
    return isDead;
}

-(void)attack:(Being *)being{

   
    being.health -= self.strength;
    NSLog(@"%@ was attacked", being.name);
    NSLog(@"%@ has %i in health left", being.name, being.health);
    
}




@end
