//
//  BeingManager.h
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-09.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FSHelper.h"
#import "Being.h"
#import "Player.h"
#import "Monster.h"

@interface BeingManager : NSObject
@property (nonatomic, strong) NSMutableArray *beings;
@property (nonatomic, strong) Player *player; //Game will have at least one player sooner or later
@property (nonatomic, strong) FSHelper *fsHelper; //helps loading saved beings


+(BeingManager*)sharedInstance;
-(void)createLoadedContent:(NSArray *)array;

-(id)createBeing:(id)type withHealth:(int)health andName:(NSString*)name andStrength:(int)strength;
-(void)kill:(Being*)being;
-(void)viewBeingInfo:(NSString*)name;

@end
