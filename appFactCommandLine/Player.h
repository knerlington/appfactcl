//
//  Player.h
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-07.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "Being.h"
#import "Weapon.h"

@interface Player : Being
@property (nonatomic, weak) Weapon *currentWeapon;
@property (nonatomic, strong) NSMutableArray *weapons;

- (id)initWithDictionary:(NSDictionary *)dictionary;
-(void)printPlayerData;
-(void)changeWeapon;

@end
