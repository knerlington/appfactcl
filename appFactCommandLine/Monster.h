//
//  Monster.h
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-09.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Being.h"

@interface Monster : Being


-(void)growl;
-(void)flee;

@end
