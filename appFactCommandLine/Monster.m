//
//  Monster.m
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-09.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "Monster.h"


@implementation Monster
-(void)growl{
    //make it growl
    NSLog(@"%@ growls like a drunk out of booze!", self.name);
}
-(void)flee{
    //flee the field
    NSLog(@"%@ fled the field! You're too mighty.", self.name);
}
-(instancetype)initWith:(int)health andName:(NSString *)name andStrength:(int)strength{
    self = [super initWith:(int)health andName:(NSString *)name andStrength:(int)strength];
    self.type = @"monster";
    return self;
}


@end
