//
//  CreatureManager.m
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-07.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "CreatureManager.h"
#import "Player.h"
@interface CreatureManager()
@property (nonatomic, weak, readonly) CreatureManager *instance;
@end

@implementation CreatureManager

//sharedInstance - Class method returning an instance of the class
//call this whenever you want to handle a being
+(CreatureManager*)sharedInstance{
    static CreatureManager *instance;
    
    if(!instance){
        instance = [[self alloc]init];
        instance.creatureList = [[NSMutableArray alloc]init];
        
    }
    return instance;
}
-(void)printNameForAllCreatures{
    for (int i = 0;i < [self.creatureList count];i++) {

        NSLog(@"Name of creature with index : %i %@", i, [[self.creatureList objectAtIndex:i] name]);
        
    }
}
-(Being*)createBeingWithName:(NSString *)name andHealth:(NSNumber*)health andStrength:(NSNumber*)strength{
    Being *b = [[Being alloc]initWith:health andName:name andStrength:strength];
    [self.creatureList addObject:b];
    return b;
}

- (void)fetchAllBeings {
    // File maanger etc.
    NSLog(@"fetchAllBeings reached:");
    NSFileManager *fm = [[NSFileManager alloc]init];


    //NSString *absolutePath = @"/Users/danlakss/Documents/xcode/command line/appFactCommandLine/appFactCommandLine/Creatures.plist"; //Absolute path, avoid
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Creatures" ofType:@".plist"];
    
    //check if file exists
    if([fm fileExistsAtPath:path isDirectory:NO]) {
        NSLog(@"File exist!");
        //save content of file in array since plist is array with dictionaries
        NSArray *arr = [NSArray arrayWithContentsOfFile:path];
        NSLog(@"Contents of Creatures.plist: %@", arr);
        
        //loop through array and based on key named 'type' instantiate objects with the same type
        for (NSDictionary *dictionary in arr) {
            
//            Player *p1 = [[Player alloc] initWithDictionary:dictionary];
//            [self.creatureList addObject:p1];
            
            
//            NSString *type = [dictionary objectForKey:@"type"];
//            if([type isEqualToString:@"Player"]){
//                //create player
//                Player *p = [[Player alloc]initWithName:[dictionary objectForKey:@"name"]];
//                [self.creatureList addObject:p];
//                
//            }
            
            Player *p = [Player new];
            //auto-assign properties from plist
            //make sure that property exists before setting or make it conditional
            NSArray *arrayWithKeys = [dictionary allKeys];
            for(NSString *key in arrayWithKeys){
                [p setValue:[dictionary objectForKey:key] forKey:key];
            }
            
            
                
            //NSString *type1 = dictionary[@"type"]; // Only immutable object copies
        }
        
//        NSDictionary *d = nil;
//        
//        for (NSString *key in d.allKeys) {
//            id obj = [d objectForKey:key];
//        }
        
    }else{
        NSLog(@"File doesn't exist!");
    }
    
}

-(void)instantiatePlistObjFromArray:(NSArray*)array{
//    for(id obj in array){
//        //do something
//    }
}



@end
