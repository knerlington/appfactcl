//
//  FSHelper.m
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-09.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "FSHelper.h"

@implementation FSHelper

-(id)init{
    self = [super init];
    self.fm = [[NSFileManager alloc]init];
    return self;
}

//loads file from documents folder with passed fileName
-(NSString*)loadFromFileName:(id)fileName{
    NSURL *url = [self getDocumentsPath];
    
    NSString *docDir = [NSString stringWithFormat:@"%@", url];
    NSString *filePath = [docDir stringByAppendingString:fileName];
    return filePath;
}

-(NSArray*)loadContentFromFileWithName:(NSString *)fileName{

//File path through NSBundle
    NSString *path = [[NSBundle mainBundle]pathForResource:fileName ofType:@".plist"];
//Check if file exists
    NSArray *array;
    if([self.fm fileExistsAtPath:path isDirectory:NO]){
        //File exist
        array = [NSArray arrayWithContentsOfFile:path];
        NSLog(@"File exist and have been returned!");
        

    }else{
        //File doesn't exist
        NSLog(@"File doesn't exist!!!");
    }

    return array;
}

//returns path for resources copied to app bundle
-(NSString*)getPathForFileFromBundle:(NSString *)file{
    NSString *path = [[NSBundle mainBundle]pathForResource:file ofType:@".plist"];
    
    return path;
}

//returns the users documents folder
//append to this when creating save files
-(NSURL *)getDocumentsPath{
    NSArray *documentsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsRoot = [documentsArray objectAtIndex:0];
    NSURL *url = [NSURL URLWithString:documentsRoot];
    NSLog(@"%@", documentsRoot);
    return url;
}

//saves player data from local method scope
//data is saved to doc folder fetched from -(NSURL*)getDocumentsPath
-(void)saveToFileName:(NSString *)fileName withData:(id)object{
    //get url and create string from it
    NSURL *url = [self getDocumentsPath];
    NSLog(@"data should be saved to doccuments folder");
    NSLog(@"URL to doc folder: %@", url);
    
    NSString *docDir = [NSString stringWithFormat:@"%@", url];
    NSLog(@"doc dir as string: %@", docDir);
    
    NSString *file = [docDir stringByAppendingString:fileName];
    NSLog(@"file path: %@", file);
    
    //save data
    NSArray *keys = [[NSArray alloc]initWithObjects:@"type", @"name", @"health", @"strength",@"weapons" ,nil];
    NSDictionary *dict = [object dictionaryWithValuesForKeys:keys];
    
    //save dict in array
    NSMutableArray *dictToArray = [[NSMutableArray alloc]initWithObjects:dict, nil];
    
    NSLog(@"array saved with player data: %@", dict);
    [dictToArray writeToFile:file atomically:YES];
}

-(void)saveContentToDictionary{
    //Load the plist you want to save to
    NSArray *tempArray = [NSArray arrayWithContentsOfFile:[self getPathForFileFromBundle:@"Beings"]];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjects:tempArray forKeys:tempArray];
    
    NSArray *documentsDirArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    NSString *documentsPath = [documentsDirArray objectAtIndex:0];
    [documentsPath stringByExpandingTildeInPath];
    NSString *fileToBeSaved = [documentsPath stringByAppendingString:@"/save.plist"];

    
    NSLog(@"Save log: %hhd", [dict writeToFile:[self getPathForFileFromBundle:fileToBeSaved] atomically:YES]);
    
}

@end
