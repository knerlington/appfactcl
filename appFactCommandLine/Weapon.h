//
//  Weapon.h
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-07.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Weapon : NSObject
@property (nonatomic) int damage;
@property (nonatomic, strong) NSString* name, *type;
-(instancetype)initWithName:(NSString*)name andDamage:(int)damage;
@end
