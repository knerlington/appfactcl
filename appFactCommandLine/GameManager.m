//
//  GameManager.m
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-07.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

//GameManager
//Inits and lets you play the game

#import "GameManager.h"
#import "CreatureManager.h"
#import "Player.h"
#import "Monster.h"

@implementation GameManager

-(void)createNewGame{
    //add logic here
}

-(void)initialize{
    //init properties etc.
    self.beingManager = [BeingManager sharedInstance];
    NSLog(@"supeRPG started");
    NSLog(@"Would you like to load a saved game or start a new one?");
    
    
    
    NSLog(@"1. New game");
    NSLog(@"2. Load game");
    
    
    char charList[1];
    scanf("%s", charList);
    NSString *input = [NSString stringWithCString:charList encoding:1];
    int nr = [input intValue];
    NSArray *tempArray = nil;
    switch (nr) {
        case 1:
        {
            //start new game
            NSLog(@"New game chosen!");
            //load beings from bundle
            tempArray = [NSArray arrayWithContentsOfFile:[self.beingManager.fsHelper getPathForFileFromBundle:@"Beings"]];
            [self.beingManager createLoadedContent:tempArray];
            [self presentOptions];
            break;
    }
        case 2:
    {
            //load game
            NSLog(@"Load game chosen!");
            //tempArray = [NSArray arrayWithContentsOfFile:[self.beingManager.fsHelper loadFromFileName:@"/supersave.plist"]];
            NSDictionary *d = [NSDictionary dictionaryWithContentsOfFile:[self.beingManager.fsHelper loadFromFileName:@"/supersave.plist"]];
            [self.beingManager createLoadedContent:d];
            [self presentOptions];
            break;
    }
        default:
            break;
    }
}



-(void)presentOptions{
    //add player options here
    
    NSLog(@"--What would you like to do?--");
    NSLog(@"View character data: 1");
    NSLog(@"Fight a random monster: 2");
    NSLog(@"FOR TESTING ONLY - Decrease player health: 3");
    NSLog(@"Save current player stat: 4");
        NSLog(@"Change weapon: 5");
    
    char charList[1];
    scanf("%s", charList);
    NSString *input = [NSString stringWithCString:charList encoding:1];
    int nr = [input intValue];
    switch (nr) {
        case 1:{
            //view char data
            [self.beingManager viewBeingInfo:self.beingManager.player.name];
            [self.beingManager.player printPlayerData];
            [self presentOptions];
            break;
        }

        case 2:
            //look for monsters
            [self searchForMonsters];
            break;
        case 3:
            //test for changing player value
            //testing save functionality
            self.beingManager.player.health--;
            [self presentOptions];
            break;
        case 4:
            
            //save game
            [self.beingManager.fsHelper saveToFileName:@"/supersave.plist"withData:self.beingManager.player];
            [self presentOptions];
            break;
        case 5:
            //change weapon
            [self.beingManager.player changeWeapon];
            
        default:
            break;
    }
}

-(void)searchForMonsters{
    //monster searching goes here
    //random int test
    int interval = 10;
    int newNr = arc4random() % interval;
    NSLog(@"newNr: %i", newNr);
    if(newNr <= 5){
        //monster doesnt appear
        NSLog(@"You didn't find any monsters!");
        [self presentOptions];
    }else if(newNr > 5){
        //monster appears
        NSLog(@"Monster appeared!");
        
        //create a new monster
        Monster *monster = [[Monster alloc]initWith:10 andName:@"smallMonster" andStrength:2];
        [self.beingManager.beings addObject:monster];
        [self fight];
    }
        
}

-(void)fight{
    //creates a fight
    NSLog(@"Fight started!");
    NSLog(@"Press 1 to hit monster");

    
    Monster *monster;
    for(Monster *m in self.beingManager.beings){
        if([m.name isEqualToString:@"smallMonster"]){
            monster = m;
            NSLog(@"Monster fetched!");
            
        }
        
    }
    
   
    char charList[1];
    scanf("%s", charList);
    NSString *input = [NSString stringWithCString:charList encoding:1];
    int nr = [input intValue];
    switch (nr) {
        case 1:{
             NSUInteger monsterIndex = [self.beingManager.beings indexOfObject:monster];
            [self.beingManager.player attack:monster];
            if([[self.beingManager.beings objectAtIndex:monsterIndex]isDead]){
                //monster is dead
                //remove monster from list
                [self.beingManager.beings removeObjectAtIndex:monsterIndex];
                NSLog(@"YOU WON THE FIGHT");
                [self presentOptions];
            }else if(![[self.beingManager.beings objectAtIndex:monsterIndex]isDead]){
                //monster isn't dead
                //hit player back
                [[self.beingManager.beings objectAtIndex:monsterIndex]attack:self.beingManager.player];
                [self fight];
            }
            
        }
            break;
            
        default:
            break;
    }
    
    
    
    
    
}



@end
